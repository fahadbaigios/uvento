//
//  DashboardViewModel.swift
//  Uvento
//
//  Created by Fahad Baig on 03/02/2021.
//

import Foundation
import Utilities
import RxSwift
import RxFlow
import RxRelay
import Store

public class DashboardViewModel: ViewModel, Stepper {
 
    public enum Cell {
        case allEventsCell(AllEventsCellViewModel)
        case popularEventsCell
    }
    
    public struct Input {}
    
    public struct Output: Loadable {
        public var loadingOutput = LoadingOutput()
        let data: Observable<UsersModelOutput?>
        let dataSource : Observable<[Section]>
    }
    
    public var input = Input()
    public var output : Output
    private var disposeBag = DisposeBag()
    public var steps = PublishRelay<Step>()
    public let store: DashboardStore
    
    enum SectionType {
        case header
        case allEvents(AllEventsCellViewModel)
        case popularEvents([PopularEventsCellViewModel])
    }
    struct Section {
        let title: String
        let type: SectionType
    }
    
    public init(store: DashboardStore) {
        self.store = store
        let dataSource = BehaviorSubject(value: [Section(title: "", type: .header), Section(title: "All Events", type: .allEvents(AllEventsCellViewModel.init(events: [AllEventsViewCellViewModel.init(icon: R.image.concert(), title: "Concert"), AllEventsViewCellViewModel.init(icon: R.image.pingPong(), title: "Sports"), AllEventsViewCellViewModel.init(icon: R.image.graduation(), title: "Education")]))), Section(title: "Popular Events", type: .popularEvents([PopularEventsCellViewModel.init(icon: R.image.sport(), title: "Sports Meet in Galaxy Field"), .init(icon: R.image.art(), title: "Art & Meet in Street Plaza"), .init(icon: R.image.music(), title: "Youth Music in Galleria")]))])
        
        output = Output(data: store.data.asObservable(), dataSource: dataSource)
        
        fetchData()
    }
    
    private func fetchData() {
        output.loading.onNext(true)
        store.fetchData().subscribe { [weak self] (_) in
            self?.output.loading.onNext(false)
        } onError: { [weak self] (error) in
            self?.output.loading.onNext(false)
            self?.output.error.onNext(error.localizedDescription)
        }.disposed(by: disposeBag)
    }

}
