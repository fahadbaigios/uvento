//
//  DashboardViewController.swift
//  Uvento
//
//  Created by Fahad Baig on 03/02/2021.
//

import UIKit
import Utilities
import Reusable
import RxSwift

class DashboardViewController: BaseViewController<DashboardViewModel>, ViewModelBased, StoryboardSceneBased  {
    public static var sceneStoryboard: UIStoryboard = R.storyboard.dashboard()

    @IBOutlet weak var tableView: UITableView!
    
    public var dataSource: [DashboardViewModel.Section] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func customizeUI() {
        configureTableView()
    }
    
    func configureTableView() {
        tableView.register(cellType: HeaderViewCell.self)
        tableView.register(cellType: AllEventsCell.self)
        tableView.register(cellType: PopularEventsCell.self)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 150
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    override func bindViewModel(vm: DashboardViewModel) {
        
        vm.output.data.subscribe(onNext: { (data) in
            guard let dataSource = data else { return }
            print("data: \(dataSource.data.count)")
            print("email: \(dataSource.data.first?.email ?? "")")
        }).disposed(by: disposeBag)
        
        vm.output.dataSource.subscribe(onNext: { [weak self] (dataSource) in
            self?.dataSource = dataSource
        }).disposed(by: disposeBag)
        
        bindLoadable(loadable: vm.output)
    }

}

extension DashboardViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch dataSource[section].type {
        case .header, .allEvents(_):
            return 1
        case .popularEvents(let events):
            return events.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch dataSource[indexPath.section].type {
        case .header:
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: HeaderViewCell.self)
//            cell.backgroundColor = .white
            return cell
        case .allEvents(let vm):
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: AllEventsCell.self)
            cell.viewModel = vm
            return cell
        case .popularEvents(let events):
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: PopularEventsCell.self)
            cell.viewModel = events[indexPath.row]
            return cell
        }
    }
    
}

extension DashboardViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 202
        } else if indexPath.section == 1 {
            return 100
        } else {
            return 148
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return (dataSource[section].title == "") ? 0 : 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionView = SectionView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        sectionView.title.text = dataSource[section].title
        return sectionView
    }
}
