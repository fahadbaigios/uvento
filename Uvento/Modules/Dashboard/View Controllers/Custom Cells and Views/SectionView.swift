//
//  SectionView.swift
//  Uvento
//
//  Created by Fahad Baig on 03/02/2021.
//

import UIKit
import Reusable
import Utilities
import RxSwift

public class SectionView: UIView, NibOwnerLoadable {
    
    //MARK: - IBOutlets
    @IBOutlet weak public var title: UILabel!
    
    //MARK: - init
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        loadNibContent()
        customizeUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadNibContent()
        customizeUI()
    }
    
    
    private func customizeUI() {
      
    }

}
