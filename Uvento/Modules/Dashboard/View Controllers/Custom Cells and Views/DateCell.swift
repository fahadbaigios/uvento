//
//  dateCell.swift
//
//  Created by Fahad Baig on 29/08/2020.
//  Copyright © 2020 Fahad Baig. All rights reserved.
//

import UIKit
import JTAppleCalendar
import Reusable

class DateCell: JTAppleCell,NibReusable {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dayName: UILabel!
    @IBOutlet var selectedView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        //self.selectedView.layer.cornerRadius = self.bounds.height/2
    }
}
