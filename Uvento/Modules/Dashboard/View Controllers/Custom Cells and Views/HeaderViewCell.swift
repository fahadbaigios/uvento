//
//  HeaderViewCell.swift
//  Uvento
//
//  Created by Fahad Baig on 03/02/2021.
//

import UIKit
import Reusable

class HeaderViewCell: UITableViewCell, NibReusable {

    @IBOutlet weak var calendarView: CalendarView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        calendarView.viewModel = CalendarViewModel.init()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state    
    }
    
}
