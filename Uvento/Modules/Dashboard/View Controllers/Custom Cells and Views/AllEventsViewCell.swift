//
//  AllEventsViewCell.swift
//  Uvento
//
//  Created by Fahad Baig on 03/02/2021.
//

import UIKit
import Utilities
import Reusable
import Store
import RxSwift

public class AllEventsViewCellViewModel: ViewModel {
    
    public struct Input {}
    
    public struct Output {
        let icon: Observable<UIImage?>
        let title: Observable<String>
    }
    
    public let input = Input()
    public let output: Output
    private var disposeBag = DisposeBag()
    
    public init(icon: UIImage?, title: String) {
        
        //Output
        output = Output(icon: Observable.just(icon), title: Observable.just(title))
        
    }
    
}

class AllEventsViewCell: BaseCollectionViewCell<AllEventsViewCellViewModel>, NibReusable {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var title: UILabel!
    
    override func customizeUI() {
        self.layer.cornerRadius = 5
        self.applyShadow(cornerRadius: 5)
    }
    
    override func bindViewModel(vm: AllEventsViewCellViewModel) {
        vm.output.icon.compactMap{$0}.bind(to: icon.rx.image).disposed(by: disposeBag)
        vm.output.title.bind(to: title.rx.text).disposed(by: disposeBag)
    }

}

extension UIView {
    
    func applyShadow(cornerRadius: CGFloat) {
        self.layer.cornerRadius = cornerRadius
        self.layer.applySketchShadow(color: UIColor.init(hex: "000000")!, alpha: 0.3, blur: 10, spread: 0)
    }
    
    func applyCornerRadius(radius: CGFloat, corners: CACornerMask) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
        self.layer.maskedCorners = corners
    }
}
