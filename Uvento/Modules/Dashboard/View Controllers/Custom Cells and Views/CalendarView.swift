//
//  CalandarView.swift
//
//  Created by Fahad Baig on 15/10/2020.
//  Copyright © 2020 Fahad Baig. All rights reserved.
//

import UIKit
import Reusable
import UIKit
import Reusable
import JTAppleCalendar
import Utilities
import RxSwift
public class CalendarViewModel:ViewModel{
    public struct Input{}
    public struct Output{
        let startDate:Date
        let endDate:Date
    }
    public var input:Input
    public var output:Output
    private var disposeBag = DisposeBag()
    public init(){
        
        //Prepare Start Date and end date for calendar View
       // let formatter = DateFormatter()
        //formatter.dateFormat = "yyyy MM dd"
        let startDate = Date()//formatter.date(from: "2020 10 1")!
        let oneYear = 12*(30*86400)
        let endDate = Date().addingTimeInterval(TimeInterval(100*oneYear))
        
        input = Input()
        output=Output(startDate: startDate, endDate: endDate)
    }
}


public class CalendarView: UIView, NibOwnerLoadable, ViewModelBased {
    
    //MARK: -Outlets
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    var diposeBag = DisposeBag()
    public var viewModel: CalendarViewModel!{
        didSet{
            guard viewModel != nil else {
                return
            }
            self.loadCalendar()
        }
    }
    let formatter = DateFormatter()
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.loadNibContent()
//        self.loadCalendar()
    }
    private func loadCalendar(){
        calendarView.register(cellType: DateCell.self)
        self.calendarView.scrollDirection = .horizontal
        self.calendarView.cellSize = 55
        self.calendarView.selectDates([Date()])
        self.calendarView.scrollToDate(Date(),preferredScrollPosition: .centeredHorizontally)
        self.calendarView.calendarDataSource = self
        self.calendarView.calendarDelegate = self
    }
}
extension DaysOfWeek {
    var title: String {
        switch  self {
        case .monday:
            return "Mon"
        case .tuesday:
            return "Tue"
        case .wednesday:
            return "Wed"
        case .thursday:
            return "Thu"
        case .friday:
            return "Fri"
        case .saturday:
            return "Sat"
        case .sunday:
            return "Sun"
        }
    }
}

extension CalendarView: JTAppleCalendarViewDataSource {
    public func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        

        return ConfigurationParameters(startDate: viewModel.output.startDate, endDate: viewModel.output.endDate,numberOfRows: 1, generateInDates: .off,generateOutDates: .off,firstDayOfWeek: .sunday)

    }
}

extension CalendarView: JTAppleCalendarViewDelegate {
    public func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell=calendar.dequeueReusableCell(for: indexPath, cellType: DateCell.self)
        configureCell(view: cell, cellState: cellState)
        return cell
    }
    
    public func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        configureCell(view: cell, cellState: cellState)
    }
    
    
    func configureCell(view: JTAppleCell?, cellState: CellState) {
        guard let cell = view as? DateCell  else { return }
        cell.dateLabel.text = cellState.text
        cell.dayName.text = cellState.day.title.uppercased()
        handleCellTextColor(cell: cell, cellState: cellState)
        handleCellSelected(cell: cell, cellState: cellState)
    }
    
    func handleCellTextColor(cell: DateCell, cellState: CellState) {
//        if cellState.dateBelongsTo == .thisMonth {
//            cell.dateLabel.textColor = UIColor.black
//        } else {
//        cell.dateLabel.textColor = Style.Color.Text.green
//        }
    }
    
    func handleCellSelected(cell: DateCell, cellState: CellState) {
        cell.selectedView.isHidden = !cellState.isSelected
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            if cellState.isSelected {
                cell.selectedView.layer.cornerRadius = 5
                cell.selectedView.clipsToBounds = true
            }
        }
    }
    
    public func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        configureCell(view: cell, cellState: cellState)
        //Fixed Library bug returning one day before selected date
        let newDate = date.addingTimeInterval(86400)
    
    }
    
    public func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        configureCell(view: cell, cellState: cellState)
    }
    
}
