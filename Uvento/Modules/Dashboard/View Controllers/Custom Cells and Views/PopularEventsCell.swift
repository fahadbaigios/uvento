//
//  PopularEventsCell.swift
//  Uvento
//
//  Created by Fahad Baig on 03/02/2021.
//

import UIKit
import Utilities
import Reusable
import Store
import RxSwift

import Utilities
import Reusable
import Store
import RxSwift

public class PopularEventsCellViewModel: ViewModel {
    
    public struct Input {}
    
    public struct Output {
        let title: Observable<String>
        let icon: Observable<UIImage?>
    }
    
    public let input = Input()
    public let output: Output
    private var disposeBag = DisposeBag()
    
    public init(icon: UIImage?, title: String) {
        
        //Output
        output = Output(title: Observable.just(title), icon: Observable.just(icon))
        
    }
    
}

class PopularEventsCell: BaseTableViewCell<PopularEventsCellViewModel>, NibReusable {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
    override func customizeUI() {
        backView.layer.cornerRadius = 5
    }

    override func bindViewModel(vm: PopularEventsCellViewModel) {
        vm.output.title.bind(to: title.rx.text).disposed(by: disposeBag)
        vm.output.icon.compactMap{$0}.bind(to: icon.rx.image).disposed(by: disposeBag)
    }
}
