//
//  AllEventsCell.swift
//  Uvento
//
//  Created by Fahad Baig on 03/02/2021.
//

import UIKit
import Reusable
import Store
import Utilities
import RxSwift

public class AllEventsCellViewModel: ViewModel {
    
    public struct Input {
        
    }
    
    public struct Output {
        let allEventsDataSource: Observable<[AllEventsViewCellViewModel]>
    }
    
    public let input: Input
    public let output: Output
    private var disposeBag = DisposeBag()
    
    public init(events: [AllEventsViewCellViewModel]) {
        
        //Input
        input = Input()
        
        //Output
        output = Output(allEventsDataSource: Observable.just(events))
        
    }
    
}
class AllEventsCell: BaseTableViewCell<AllEventsCellViewModel>, NibReusable {

    @IBOutlet weak var collectionView: UICollectionView!
    
    public var dataSource: [AllEventsViewCellViewModel] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    override func customizeUI() {
        configureCollectionView()
    }
    
    func configureCollectionView() {
        collectionView.register(cellType: AllEventsViewCell.self)
        collectionView.delegate = self
    }
    
    override func bindViewModel(vm: AllEventsCellViewModel) {
        vm.output.allEventsDataSource.bind(to: collectionView.rx.items(cellIdentifier: AllEventsViewCell.reuseIdentifier, cellType: AllEventsViewCell.self)) { (_,vm,cell) in
            cell.viewModel = vm
        }.disposed(by: disposeBag)
    }
    
}

//This section contains delegates and datasource for collection view
extension AllEventsCell: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width-25)/3, height: 102)
    }
}

