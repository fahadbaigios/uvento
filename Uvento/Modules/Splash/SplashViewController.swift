//
//  SplashViewController.swift
//
//  Created by Fahad Baig on 02/02/2020.
//  Copyright © 2020 Fahad Baig. All rights reserved.
//

import UIKit
import Utilities
import Reusable
import RxSwift

public class SplashViewController: BaseViewController<SplashViewModel>, ViewModelBased, StoryboardSceneBased  {
    public static var sceneStoryboard: UIStoryboard = R.storyboard.splash()
    
    @IBOutlet weak var getStartedBtn: UIButton!
    
    public override func customizeUI() {
        getStartedBtn.rx.tap.subscribe(onNext: { [weak self] (_) in
            self?.viewModel.steps.accept(AppStep.dashboard)
        }).disposed(by: disposeBag)

    }
    
    public override func bindViewModel(vm: SplashViewModel) {
        
    }
    
}
