//
//  SplashViewModel.swift
//
//  Created by Fahad Baig on 02/02/2020.
//  Copyright © 2020 Fahad Baig. All rights reserved.
//

import Foundation
import Utilities
import RxSwift
import RxFlow
import RxRelay

public class SplashViewModel: ViewModel, Stepper {
 
    public struct Input {}
    public struct Output {}
    
    public var input = Input()
    public var output = Output()
    private var disposeBag = DisposeBag()
    public var steps = PublishRelay<Step>()
    
    public init() {}

}
