//
//  Strings.swift
//  BBMerchant
//
//  Created by Fahad Baig on 26/10/2020.
//  Copyright © 2020 Techtix. All rights reserved.
//

import Foundation

let string: StringResources = {
    //Locale.current.languageCode == "ar" ? StringAr() : StringsEn()
    return StringsEn()
}()

protocol StringResources {
    var passwordReset: String {get}
}

class StringsEn: StringResources {
    let passwordReset = "Password Reset"
}

