//
//  Config.swift
//  Uvento
//
//  Created by Fahad Baig on 02/02/2021.
//

import Foundation
class Config {
    
    /*
    public var baseURL: URL {
        #if DEBUG
        return URL(string: "http://13.235.76.12/api/")!
        #else
        return URL(string: "http://13.235.76.12/api/")!
        #endif
    }
    */

    #if DEBUG
    static let apiKey = ""
    static let baseURL = URL(string: "https://reqres.in/api")!
//    static let keyGoogleAPI = "AIzaSyBmiIvOdQt6Y6_Rk-Z3N-IVj8dwHf_KgZM"

    #else
    static let apiKey = ""
    static let baseURL = URL(string: "https://reqres.in/api")!
//    static let keyGoogleAPI = "AIzaSyBmiIvOdQt6Y6_Rk-Z3N-IVj8dwHf_KgZM"
    #endif

}
