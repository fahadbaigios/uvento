//
//  DashboardFlow.swift
//  Uvento
//
//  Created by Fahad Baig on 03/02/2021.
//

import Foundation
import RxFlow
import RxRelay
import Rswift
import Store

class DashboardStepper: Stepper {
    let steps = PublishRelay<Step>()
    let initialStep: Step = AppStep.dashboard
}

class DashBoardFlow: BBFlow {
    
    struct Service {
        let store: DashboardStore
    }
    
    override var root: Presentable {
        return rootViewController
    }
    
    let rootViewController: UITabBarController = {
        let scene = UITabBarController()
        scene.tabBar.isTranslucent = false
        scene.tabBar.barTintColor = UIColor.init(hex: "#152F3E")
        scene.tabBar.tintColor = .clear
        scene.tabBar.unselectedItemTintColor = .clear
        scene.view.backgroundColor = UIColor.init(hex: "#152F3E")
        
        return scene
    }()
    
    private let service : Service
    
    weak var window: UIWindow!
//    let homeStepper: HomeStepper
    
    private weak var stepper: DashboardStepper!
    
    init(withWindow window: UIWindow, stepper: DashboardStepper, service: Service) {
        self.window = window
        window.rootViewController = rootViewController

//        homeStepper = HomeStepper()
        self.stepper = stepper
        self.service = service
        super.init()

    }
    
    //MARK:-  Navigation
    override func navigateToAppStep(step: AppStep) -> FlowContributors {
        switch step {
        case .dashboard:
            return setupTabScene()
        default:
            return .none
        }
    }
    
    //MARK:- Scenes
    
    private func setupTabScene() -> FlowContributors {
        //first screen
        let homeVM = DashboardViewModel(store: service.store)
        let homeVC = DashboardViewController.instantiate(withViewModel: homeVM)
        
        let homeBarItem = UITabBarItem(title: "", image: R.image.home2()?.withRenderingMode(.alwaysOriginal), selectedImage: R.image.home2()?.withRenderingMode(.alwaysOriginal))
        homeBarItem.imageInsets = UIEdgeInsets.init(top: -15, left: -20, bottom: -20, right: -50)
        homeVC.tabBarItem = homeBarItem
        
        //second and third empty screens
        let tempScene1 = UIViewController()
        let tempScene1Item = UITabBarItem(title: "", image: R.image.search()?.withRenderingMode(.alwaysOriginal), selectedImage: R.image.search()?.withRenderingMode(.alwaysOriginal))
        tempScene1Item.imageInsets = UIEdgeInsets.init(top: -5, left: -5, bottom: -5, right: -10)
        tempScene1.tabBarItem = tempScene1Item
        
        let tempScene2 = UIViewController()
        let tempScene2Item = UITabBarItem(title: "", image: R.image.star()?.withRenderingMode(.alwaysOriginal), selectedImage: R.image.star()?.withRenderingMode(.alwaysOriginal))
        tempScene2.tabBarItem = tempScene2Item
    
        self.rootViewController.setViewControllers([homeVC, tempScene1, tempScene2], animated: false)
        
        return .multiple(flowContributors: [.contribute(withNextPresentable: homeVC, withNextStepper: homeVM)])
    }
    
}

