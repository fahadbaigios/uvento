//
//  AppFlow.swift
//  Template
//
//  Created by Fahad Baig on 02/02/2018.
//  Copyright © 2018 Mansoor Ali. All rights reserved.
//

import RxFlow
import RxCocoa
import RxSwift
import UIKit
import Store

public class AppStepper: Stepper {
    public var steps = PublishRelay<Step>()
    public var initialStep: Step = AppStep.splash
    public init() {}
}

class AppFlow: BBFlow {

    override var root: Presentable {
        return rootViewController
    }

    let rootViewController: UINavigationController = {
        let scene = UINavigationController()
        scene.isNavigationBarHidden = true
        return scene
    }()

    weak var window: UIWindow!
    
    init(withWindow window: UIWindow) {
        self.window = window
        window.rootViewController = rootViewController
    }

    //MARK:-  navigation to flow
    override func navigateToAppStep(step: AppStep) -> FlowContributors {
        switch step {
        case .splash:
            return navigateToSplash()
        case .dashboard:
            return navigateToDashboardFlow()
        default:
            return .none
        }
    }

    //MARK:- Scenes
    private func navigateToSplash() -> FlowContributors {
        let vm = SplashViewModel()
        let scene = SplashViewController.instantiate(withViewModel: vm)

        rootViewController.setViewControllers([scene], animated: false)
        let flowContributor = FlowContributor.contribute(withNextPresentable: scene, withNextStepper: vm)
        return FlowContributors.one(flowContributor: flowContributor)
    }

    //MARK:- Flows

    private func navigateToDashboardFlow() -> FlowContributors {
        let stepper = DashboardStepper()
        let flow = DashBoardFlow(withWindow: window, stepper: stepper, service: .init(store: DashboardStore()))

        return .one(flowContributor: .contribute(withNextPresentable: flow, withNextStepper: stepper))
    }

}

