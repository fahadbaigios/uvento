//
//  AppStep.swift
//  BBMerchant
//
//  Created by Fahad Baig on 02/02/2020.
//  Copyright © 2020 Techtix. All rights reserved.
//


import RxFlow

public enum AppStep: Step {

    case splash
    case dashboard

}

