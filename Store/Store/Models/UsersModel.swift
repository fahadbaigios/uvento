//
//  UsersModel.swift
//  Store
//
//  Created by CTS Move on 03/02/2021.
//

import Foundation

public final class UsersModelOutput: Codable {
   
    public var page: Int = -1
    public var perPage: Int = -1
    public var total: Int = -1
    public var totalPages: Int = -1
    public var data: [UserData] = []
    public var support: Support?
  
    public init() {}
        
}

public class UserData: Codable {

    public var id: Int = -1
    public var email: String = ""
    public var firstName: String = ""
    public var lastName: String = ""
    public var avatar: String?

    public init() {}
}

public class Support: Codable {
    
    public var url: String = ""
    public var text: String = ""
    
    public init() {}
}
