//
//  DataStore.swift
//  Store
//
//  Created by Fahad Baig on 03/02/2021.
//

import Foundation
import RxSwift
import Moya
import UIKit
import RxRelay

public final class DashboardStore {
 
    public var data = BehaviorRelay<UsersModelOutput?>(value: nil)
    
    public init() {}
    
    //Fetch data
    public func fetchData() -> Single<Void> {
        let request: Single<UsersModelOutput> = NetworkManager.shared.newRequest(method: .get, task: .requestParameters(parameters: ["page": 2], encoding: URLEncoding.default), path: "/users")
        return request.do(onSuccess: {[weak self] (data) in
            self?.data.accept(data)
        }).map{_ in ()}
    }
    
}
